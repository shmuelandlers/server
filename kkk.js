useEffect(() => {
    // POST request using fetch inside useEffect React hook
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ title: 'React Hooks POST Request Example' })
    };
    fetch('https://jsonplaceholder.typicode.com/posts', requestOptions)
        .then(response => response.json())
        .then(data => setPostId(data.id));

    // empty dependency array means this effect will only run once (like componentDidMount in classes)
}, []);




router.get("/member/:id", (req, res) => {

    const count = members.some(member => member.id === parseInt(req.params.id));
    if (count) {
        res.json(members.filter(member => member.id === parseInt(req.params.id)));
    } else {
        res.status(404).json({ msg: "you made an erorr" });
    }
});


router.put("/member2/:id", (req, res) => {

    const count = members.some(member => member.id === parseInt(req.params.id));

    if (count) {
        const upMember = req.body;

        members.forEach(member => {
            if (member.id === parseInt(req.params.id)) {
                member.name = upMember.name ? upMember.name : member.name
                member.email = upMember.email ? upMember.email : member.email

                res.json({ msg: "updated" })
            }
        });
    } else {
        res.status(404).json({ msg: "you made an erorr" });
    }
});

//  res.sendFile(__dirname, "template", "g.html");
// res.sendFile(path.join(__dirname, 'template', 'demo.html'));