const express = require('express');
const members = require('./members');
const session = require('express-session');
const router = express.Router();
const mysql = require('mysql');;
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
//const { rows } = require('mssql');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require("cors");

var saltRounds = 10;

var count = 0;
var localStorage = [];
var currentUser = [];

router.use(cors({
    origin: ["http://localhost:3000"],
    methods: ["GET", "POST", "DELETE"],
    credentials: true
}));

router.use(cookieParser());
router.use(bodyParser.urlencoded({ extended: true }))

router.use(express.json());
router.use(express.urlencoded({ extended: false }));

router.use(session({
    key: "userId",
    secret: "secret",
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 60 * 60 * 24
    }
}))

const connection = mysql.createPool({
    port: 3306,
    host: '127.0.0.1',
    user: 'root',
    password: '123456',
    database: 'helloworld',
});
connection.getConnection((err, conn) => {
    if (err) {
        throw err;
    } else {
        console.log("Connected to my mySQL ");
    }

});

function counter() {
    count = count + 1
    console.log(count)
}


const verifyJwt = (req, res, next) => {
    const token = req.headers["x-access-token"];
    if (!token) {
        res.send("we need a token");
    } else {
        jwt.verify(token, "jwtSecret", (err, decoded) => {
            if (err) {
                res.json({ auth: false, msg: "authentication failed" });
            } else {
                req.userId = decoded.id;
                next();
            }
        });
    }
}


router.post("/register", (req, res) => {
    const username = req.body.username;
    const password = req.body.password;

    connection.query(`SELECT * from usernames WHERE username = '${username}'`, (error, rows, field) => {
        if (rows < 1) {

            bcrypt.hash(password, 10, (err, hash) => {

                connection.query(`INSERT INTO usernames (username, password) VALUES ('${username}','${hash}')`,
                    (err, rows, fields) => {
                        console.log(rows);
                        res.json({ result: rows })
                    }
                );

            });

        } else {
            console.log("cant add user try again")

        }
    })
})






router.get("/getmember", verifyJwt, (req, res) => {

    connection.query(`SELECT * from hello WHERE username2 = '${currentUser.username}'`, (error, rows, field) => {
        if (!!error) {
            console.log("error in query");
        } else {
            res.json(rows);
            extractor(rows);
        }
    });
});

function extractor(value) { localStorage = value; }

router.post('/postmember', verifyJwt, (req, res) => {
    var username2 = currentUser.username;
    var { name, email } = req.body;

    if (username2 == null) {
        res.status(401).json({ msg: "you must be signed in to post" });
    } else {
        var values = [
            [name, email, username2]
        ];
    }

    function queryFunction() {
        connection.query(
            `INSERT INTO hello (name,email,username2) VALUES ?`, [values],
            function (err, result, fields) { }
        );
    }
    if (!name || !email) {
        res.status(401).json({ msg: "you must provide a name or email" });
    } else { queryFunction(); }
});


router.delete("/member3/:id", verifyJwt, (req, res) => {

    const { id } = req.params;
    var idSpliter = id.split(",");
    var idMapping = idSpliter.map(i => Number(i));
    for (var hh of idMapping) {
        deleteQuery()
    }

    function deleteQuery() {
        connection.query("DELETE FROM hello WHERE id = ?", [hh], function (error, rows, field) {
            if (error) {
                throw error;
            } else {
                res.send(rows);
            }
        });
    }
});


router.post("/loginAuth", (req, res) => {
    var username = req.body.username;
    var password = req.body.password;
    connection.query(`SELECT * from usernames WHERE username = '${username}'`,
        (error, rows, field) => {
            if (error) {
                console.log("error in query");
            } else {
                bcrypt.compare(password, rows[0].password, (error, response) => {
                    if (response) {

                           req.session.user = rows
                        console.log("you are signed in")

                        try {
                            const user = rows[0].id;
                            console.log(user);
                            var token = jwt.sign({ user }, "jwtSecret", {
                                expiresIn: 300
                            });

                            usernameExtractor(rows[0])
                            res.json({ auth: true, token: token, result: rows });
                        } catch (error) {
                            res.json({ auth: false, msg: "you are not authenticated" });
                        }

                    } else {
                        console.log("are made an error")
                    }
                })
            }
        });
});

function usernameExtractor(params) { currentUser = params; }

module.exports = router;